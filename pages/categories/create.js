import React, {useContext,useState} from 'react'
import View from '../../components/View'; 
import {Row , Col, Button, Form, Card, Container} from 'react-bootstrap'
import UserContext from '../../UserContext'
import Router from "next/router";
import AppHelper from '../../app-helper';



export default function create() {
    return ( 
        <View title={ 'Budget Tracker' }>
            <Row className="justify-content-center">
                <Col xs md="12">
                    <h3 className="text-center">New Category</h3>
                    <CreateForm /> 
                </Col>
            </Row>
        </View>
    )
}

const CreateForm = () => {  
    const { user } = useContext(UserContext);

    const [categoryName, setCategoryName] = useState('')
    const [categoryType, setCategoryType] = useState('')

    function addCategories(e){
        e.preventDefault()
        if(user.id){
            fetch(`${ AppHelper.API_URL }/users/categories`, {
                "method": "POST",
                "headers": {
                    'Content-Type' : 'application/json',
                    Authorization : `Bearer ${localStorage.getItem('token')}`
                },
                body: JSON.stringify({
                    categoryType: categoryType,
                    categoryName: categoryName
                })
            })
            .then(res => res.json())
            .then(data => {
                console.log(data)
                if (data === true){
                    alert("Successfully added to category")
                    Router.reload('/categories')
                } else {
                return(
                    alert("Successfully added to category")   
                )    
                }
            })
            
        }
}




    return (
        <React.Fragment>
        <Container className= "w-50">
            <Card>
                <Card.Body>
                <Form onSubmit={(e) => addCategories(e)}>
                    <Form.Group>
                        <Form.Label>Category Name</Form.Label>
                        <Form.Control 
                        type="text"
                        placeholder="Enter category name" 
                        value= {categoryName}
                        onChange={e => setCategoryName(e.target.value)}
		                required
                        />
                    </Form.Group>

                    <Form.Group controlId="exampleForm.ControlSelect1">
                        <Form.Label>Category Type</Form.Label>
                        <Form.Control 
                        as="select" 
                        type="text" 
                        onChange={ e => setCategoryType(e.target.value)}
                        required
                        >
                        <option default> Select Category </option>
                        <option value ="Income" >Income</option>
                        <option value ="Expenses" >Expenses</option>
                        </Form.Control>
                    </Form.Group>
                    <Button variant="primary" type="submit" >
                        Submit
                    </Button>
                </Form>
                </Card.Body>
            </Card>
        </Container>  
        
        </React.Fragment> 
    )

}


