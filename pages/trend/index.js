import React, {useState,useEffect, useContext} from 'react'
import View from '../../components/View';
import {Row,Col,InputGroup} from 'react-bootstrap'
import UserContext, { UserProvider } from '../../UserContext'
import {Line} from 'react-chartjs-2'
import moment from 'moment'


export default function index() {
    return ( 
        <View title={ 'Budget Tracker' }>
            <Row className="justify-content-center">
                <Col xs md="12">
                    <h3>Balance Trend</h3>
                    <BalanceTrend /> 
                </Col>
            </Row>
        </View>
    )
}
    
    
    
const BalanceTrend = () => {
    const { user } = useContext(UserContext);
    const [startDate, setStartDate] = useState("");
    const [endDate, setEndDate] = useState ("");
    const [trendDates, setTrendDates] =useState("");
    const [balanceRecords, setBalanceRecords] = useState("");
    const [records, setRecords] =useState([])

    useEffect (() => {
            
        fetch(`${ AppHelper.API_URL }/users/details`,{
            headers: {
                'Content-Type' : 'application/json',
                Authorization : `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {    
            setRecords(data.record)
        })
    },[])

        useEffect(()=>{
            let datesArr = [];
            let total = 0;
            let balance = [];
            if(user.id){
                records.forEach(records => {
                    if((moment(records.createdOn).format("YYYY-MM-DD") >= startDate) && (moment(records.createOn).format("YYYY-MM-DD") <= endDate)){
                        if(datesArr.includes(moment(records.createdOn).format("YYYY-MM-DD")) != true){
                            console.log(moment(records.createdOn).format("YYYY-MM-DD"))
                            if(records.categoryType === "Income"){
                                total = total + records.amount
                                balance.push(total)
                            }
                            if(records.categoryType === "Expenses"){
                                total = total - records.amount
                                balance.push(total)
                            }
                            datesArr.push(moment(records.createdOn).format("YYYY-MM-DD"))
                            console.log(datesArr)
                        }else{
                            if(records.categoryType === "Income"){
                                total = balance[balance.length - 1] + records.amount
                                balance.pop()
                                balance.push(total)
                            }
                            if(records.categoryType === "Expenses"){
                                total = balance[balance.length - 1] - records.amount
                                balance.pop()
                                balance.push(total)
                            }
                        }
                
                    }

                    setTrendDates (datesArr);
                    setBalanceRecords (balance);
                })
            }

        }, [startDate,endDate])  
    
        console.log(trendDates)
        console.log(balanceRecords)

         

    

        const data = {
            labels: trendDates,
            datasets: [{
                label: 'Total Balance per Day',
                backgroundColor: 'rgba(255,99,132,0.2)',
                borderColor:'rgba(255,99,132,1)',
                data: balanceRecords
            }]
        }




    return (
        <React.Fragment>
            <InputGroup>

                <h6>Start Date: <input type= "date" value={startDate} onChange={e => setStartDate(e.target.value)} /></h6>

                <h6>End Date: <input type= "date" value= {endDate} onChange={e => setEndDate(e.target.value)} /> </h6>

            </InputGroup>

            <Line data = {data} />

        </React.Fragment>
    )
}    
    
    

