import React, {useContext, useState, useEffect} from 'react';
import { Form, Button, Card, Container,Row,Col } from 'react-bootstrap';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import View from '../components/View';
import AppHelper from '../app-helper';
import Router from "next/router";

//Google Login
import { GoogleLogin } from 'react-google-login';

export default function Login() {
    
    return ( 
        <View title={ 'Budget Tracker' }>
            <Row className="justify-content-center">
                <Col xs md="6">
                    <h3>Budget Tracker</h3>
                    <p>Login in by using your Google Account or your Registered Email.</p>
                    <LoginForm />
                </Col>
            </Row>
        </View>
    )

}



const LoginForm = () => {    
    const { user, setUser } = useContext(UserContext);

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isActive, setIsActive] = useState(false);
    

    function authenticate(e) {
        //prevent redirection via form submission
        e.preventDefault();
        

        fetch(`http://localhost:4000/api/users/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(AppHelper.toJSON)
        .then(data => {
            console.log(data);

            if (typeof data.accessToken !== 'undefined') {
                localStorage.setItem('token', data.accessToken)
                retrieveUserDetails(data.accessToken)
            } else {
                if(data.error === 'does-not-exist'){
                    Swal.fire('Authentication Failed', 'User does not exist.', 'error')
                } else if (data.error === 'incorrect-password'){
                    Swal.fire('Authentication Failed', 'Password is incorrect.', 'error')
                } else if (data.error === 'login-type-error'){
                    Swal.fire('Login Type Error', 'You may have registered throug a different login procedure, try alternative login procedure.', 'error')
                }
            }
        })
    } 

// Google Authentication
const authenticateGoogleToken = (response) => {
    console.log(response)

    fetch(`http://localhost:4000/api/users/verify-google-id-token`, {
        method: 'POST',
        headers: {
            'Content-Type' : 'application/json'
        },
        body: JSON.stringify({
            tokenId: response.tokenId
        })
    })
    .then(AppHelper.toJSON)
    .then(data => {
        console.log(data)

        if(typeof data.accessToken !== 'undefined'){

            localStorage.setItem('token', data.accessToken)
            retrieveUserDetails(data.accessToken)

        } else {

            if(data.error === 'google-auth-error'){
                Swal.fire(
                    'Google Auth Error', 
                    'Google authenctuication procedure failed', 
                    'error'
                )
            } else if (data.error === 'login-type-error'){
                Swal.fire(
                    'Login Type Error', 
                    'You may have registered through a different login procedure', 
                    'error'
                )
            }


        }
    })
};


    const retrieveUserDetails = (accessToken) => {
        console.log(accessToken)

        fetch(`http://localhost:4000/api/users/details`, {
            headers: {
                Authorization: `Bearer ${accessToken}`
            }  
        })
        .then(AppHelper.toJSON)
        .then(data => {

            console.log(data);
            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })

			Router.push('/home');

        })

    }
        useEffect(() => {
            if((email !== '' && password !== '')){
                setIsActive(true);
            } else {
                setIsActive(false);
            }
        }, [ email, password ])
    
        return(
            <Container>
                <Card>
                    <Card.Body>
                        <Form onSubmit={authenticate}>
                            <Form.Group controlId="userEmail">
                                <Form.Label>Email Address</Form.Label>
                                <Form.Control 
                                    type="email" 
                                    value={ email }
                                    onChange={ (e) => setEmail(e.target.value) } 
                                    required
                                />
                            </Form.Group>
                            <Form.Group controlId="password">
                                <Form.Label>Password</Form.Label>
                                <Form.Control 
                                    type="password" 
                                    value={ password } 
                                    onChange={ (e) => setPassword(e.target.value) } 
                                    required
                                />
                            </Form.Group>
                            <Button 
                                className="mb-2 bg-primary w-100"
                                type="submit" 
                                block
                            >
                                Login
                            </Button>

                            <GoogleLogin                                                                                           
                            clientId='802333644669-lmmd63hiceb0a0sfrc900e7ondhv56f2.apps.googleusercontent.com'
                            buttonText="Login"
                            onSuccess={ authenticateGoogleToken }
                            onFailure={ authenticateGoogleToken }
                            cookiePolicy={ 'single_host_origin' }
                            className="w-100 text-center d-flex justify-content-center"
                            />

                            <p className="text-center">If you haven't registered yet,<a href="/register">Sign up here!</a></p>

                            

                        </Form>
                    </Card.Body>
                </Card>
            </ Container>
        )
    
}